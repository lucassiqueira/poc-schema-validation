import React, { Component } from "react";

// import { Container } from './styles';
import colorSchema from "./colorSchema.js";
import colorValidation from "./colorValidation";

export default class Colors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colors: [],
      color: "",
      email: ""
    };
    this.handleIdentifyColors = this.handleIdentifyColors.bind(this);
    this.handleAddColor = this.handleAddColor.bind(this);
    this.handleIdentifyEmail = this.handleIdentifyEmail.bind(this);
  }

  handleIdentifyColors(e) {
    const color = e.target.value;
    // switch (color) {
    //   case "red":
    //     this.setState({ color: "red" });
    //     break
    //   case "blue":
    //     this.setState({ color: "blue" });
    //     break
    //   case "green":
    //     this.setState({ color: "green" });
    //     break
    //   case "black":
    //     this.setState({ color: "black" });
    //     break
    //   case "purple":
    //     this.setState({ color: "purple" });
    //     break
    //   default:
    //     this.setState({ color: "yellow" });
    // }
    const colorValue = colorSchema[color] ? colorSchema[color] : null;

    this.setState({ color: colorValue });
  }

  handleIdentifyEmail(e) {
    const email = e.target.value ? e.target.value : null;
    if (email) {
      this.setState({ email });
    }
  }

  async handleAddColor() {
    const data = {
      id: Math.random(),
      name: this.state.color,
      email: this.state.email
    };
    const isValid = await colorValidation.isValid(data);
    if (isValid) {
      this.setState({
        colors: [...this.state.colors, data]
      });
    } else {
      console.log("Erro ao inserir nova cor");
    }
  }

  render() {
    return (
      <>
        <ul>
          {this.state.colors.map(color => (
            <li key={color.id}>
              {color.name}
              <div
                style={{
                  width: "20px",
                  height: "20px",
                  backgroundColor: color.name
                }}
              />
            </li>
          ))}
        </ul>
        <h2>Cadastro de cores</h2>
        <div>
          <input
            placeholder="Digite o email"
            onChange={this.handleIdentifyEmail}
          />
        </div>
        <div>
          <input
            placeholder="Digite a cor desejada"
            onChange={this.handleIdentifyColors}
          />
          <button onClick={this.handleAddColor}>Adicionar</button>
        </div>
      </>
    );
  }
}
