const colorSchema = {
  "red": "red",
  "blue": "blue",
  "green": "green",
  "black": "black",
  "purple": "purple",
  "yellow": "yellow"
}

export default colorSchema