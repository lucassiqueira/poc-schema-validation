import * as yup from "yup";
import colorSchema from "./colorSchema";

const schema = yup.object().shape({
  id: yup.number().required(),
  name: yup
    .string()
    .oneOf(Object.keys(colorSchema))
    .required(),
  email: yup
    .string()
    .email()
    .required()
});

export default schema;
