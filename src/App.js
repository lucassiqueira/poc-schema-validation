import React from 'react';

import Colors from './components/Colors/index'

function App() {
  return (
    <div className="App">
      <Colors/>
    </div>
  );
}

export default App;
